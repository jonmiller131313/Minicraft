use std::time::{Duration, Instant};

use sdl2::{event::Event as SdlEvent, EventPump, Sdl};

const FRAME_DURATION: Duration = Duration::from_millis(1000 / 30);

pub enum Event {
    UiInput(SdlEvent),
    UiTick,
}

pub struct EventReader {
    pump: EventPump,
    next_frame: Instant,
}
impl EventReader {
    pub fn new(sdl: &Sdl) -> Self {
        Self {
            pump: sdl.event_pump().expect("unable to get SDL event pump?"),
            next_frame: Instant::now(),
        }
    }

    /// Checks if we should update the UI, or calculate the difference between the last time we did to wait for a user
    /// event.
    pub fn read_event(&mut self) -> Event {
        self.next_frame
            .checked_duration_since(Instant::now())
            .and_then(|timeout| {
                self.pump
                    .wait_event_timeout(timeout.as_millis() as _)
                    .map(Event::UiInput)
            })
            .unwrap_or_else(|| {
                self.next_frame += FRAME_DURATION;
                Event::UiTick
            })
    }
}
